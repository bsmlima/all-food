const ADDRESS_HOST = "http://localhost:8080/address/insert";
const CLIENT_HOST = "http://localhost:8080/client/insert";
const SIGN_IN_ELEMENT = `<li class="nav-item">
                            <a class="nav-link" href="#" data-toggle="modal" data-target="#signInModal">Entrar</a>
                        </li>`;

const SIGN_UP_ELEMENT = `<li class="nav-item">
                            <a class="nav-link" href="#" data-toggle="modal" data-target="#signUpModal">Cadastrar-se</a>
                        </li>`;

const CART_ELEMENT =    `<li class="nav-item">
                            <a class="nav-link" href="/app/cart.html"><span class="fa fa-shopping-cart" id="cart-icon"></span></a>
                        </li>`;

const ACCOUNT_OPTIONS_ELEMENT = `<li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown2" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown2" id="accountDropDown"></div>
                                </li>`;

const LOGOUT_ELEMENT = `<a class="dropdown-item" href="#" onclick="logout()">Sair</a>`;

const MY_PURCHASES_ELEMENT = `<li class="nav-item">
                                  <a class="nav-link" href="/app/myPurchases.html">Minhas compras</a>
                              </li>`;


function register () {

    // Dados cadastrais
    var name = $("#signUpName").val();
    var cpf = $("#signUpCpf").val();
    var email = $("#signUpEmail").val();
    var password = $("#signUpPassword").val();

    // Dados para envio
    var country = $("#signUpCountry").val();
    var state = $("#signUpState").val();
    var city = $("#signUpCity").val();
    var neighborhood = $("#signUpNeighborhood").val();
    var street = $("#signUpStreet").val();
    var houseNumber = $("#signUpHouseNumber").val();
    var cep = $("#signUpCep").val();

    
    if (!name || !cpf || !email || !password || !country || !state || !city || !neighborhood || !street || !houseNumber || !cep) {
        displayMessage("Preencha todos os campos para continuar");
    } else {
        var data = {
            country: country,
            state: state,
            city: city,
            neighborhood: neighborhood,
            street: street,
            houseNumber: houseNumber,
            cep: cep
        }
        $.get(ADDRESS_HOST, data,  function(data) {
            insertClient(name, cpf, email, password, data.id);
        })
        .fail(function() {
            displayMessage("Ocorreu um erro :(");
        });
    }
}

function insertClient(name, cpf, email, password, addressId) {
    var data = {
        name: name,
        cpf: cpf,
        email: email,
        password: password,
        addressId: addressId,
        paymentType: 1,
        autenticated: false
    }
    $.get(CLIENT_HOST, data,  function(client) {
        login(client.email, client.password);
    })
    .fail(function() {
        displayMessage("Ocorreu um erro :(");
    });
}

$(document).ready(function () {
    $('#signUpBtn').click(function () {
        register();
    });

    $('#signInBtn').click(function () {
        var email = $("#signInEmail").val();
        var password = $("#signInPassword").val();

        login(email, password);
    });
    
    $('#signIn').bind('keypress', function(e){
        if(e.keyCode == 13) {
            var email = $("#signInEmail").val();
            var password = $("#signInPassword").val();
            login(email, password);
        }
    });

    $('#signUp').bind('keypress', function(e){
        if(e.keyCode == 13) {
            register();
        }
    });

    $('#search').val(findGetParameter('search'));

    if (!isAdmin())
        $('#right-bar').append(CART_ELEMENT);
        
    if (isLoggedIn()) {
        if (!isAdmin() && isLoggedIn()) {
            $('#left-bar').append(MY_PURCHASES_ELEMENT);
        }
        $('#right-bar').append(ACCOUNT_OPTIONS_ELEMENT);
        $('#navbarDropdown2').text(getUserName());
        $('#accountDropDown').append(LOGOUT_ELEMENT);
    } else {
         $('#right-bar').append(SIGN_IN_ELEMENT);
         $('#right-bar').append(SIGN_UP_ELEMENT);
    }
});