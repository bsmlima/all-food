const CREATE_REVIEW = ` <br>
                        <form id="insertReview" class="col-md-12">
                            <h6>Envie sua avaliação!</h6><br>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label for="new-review-score">Nota:</label>
                                </div>
                                <div class="col-md-9">
                                    <input type="number" class="form-control col-md-2" id="new-review-score" step="1" min="1" max="5" value="5">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label for="new-review-title">Título:</label>
                                </div>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" id="new-review-title">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-md-3">
                                    <label for="new-review-opinion">Opinião:</label>
                                </div>
                                <div class="col-md-9">
                                    <textarea class="form-control" rows="5" id="new-review-opinion" placeholder="Opinião"></textarea>
                                </div>
                            </div>
                            <div class="form-group row justify-content-end">
                                <button type="button" class="btn" id="send-review">Enviar</button>
                            </div>
                        </form>`;

function showProduct(id) {
    var url = PRODUCT_URL + '/getbyid';
    var data = {
        id: id
    }
    jQuery.ajaxSetup({async:false});
    $.get(url, data,  function(product) {
        $('#productName').text(product.name);
        var productHtml = '<div class="col-md-12"><div class="product" style="height: 350px;"> ';
        productHtml += '<h6 class="product-title">' + product.name + '</h6>';
        productHtml += '<p>R$ ' + product.price + '</p>';
        if (isAdmin()) {
            productHtml += '<p>Estoque: ' + '<input id="quantity" class="form-control form-control-sm item-quantity" type="number" step="1" min="0" value="' + product.quantityInStock + '">' + ' unidades</p>';
        } else {
            productHtml += '<p>Estoque: ' + product.quantityInStock + ' unidades</p>';
        }

        productHtml += '<p>Categoria: ' + product.category.name + '</p>';
        productHtml += '<p>Alérgicos:</p>';
        productHtml += '<ul>';
        $.each(product.restrictions, function (key, restriction) {
            productHtml += '<li>' + restriction.name + '</li>';
        });
        productHtml += '</ul>';
        if (isAdmin()) {
            productHtml += `<div class="button-container col-md-12"
                                <div class="row justify-content-center">
                                    <button type="button" class="btn col-md-8" id="view-history">Ver histórico</button>
                                </div>
                            </div>`;
        } else {
            productHtml += `<div class="button-container col-md-12"
                                <div class="row justify-content-center">
                                    <button type="button" class="btn col-md-8" id="addToCart">Adicionar ao carrinho</button>
                                </div>
                            </div>`;
        }
        productHtml += '</div></div>';
        $('#productInfo').html(productHtml);
    })
    .fail(function() {
        displayMessage("Ocorreu um erro ao buscar o produto");
    });
    jQuery.ajaxSetup({async:true});
}

function updateQuantity(productId, quantity) {
    var url = PRODUCT_URL + "/update"
    var data = {
        productId: productId,
        quantityInStock: quantity
    }
    
    $.get(url, data,  function(data) {
        console.log("Estoque atualizado com sucesso");
    })
    .fail(function() {
        displayMessage("Ocorreu um erro :(");
    });
}

function getReviews(productId) {
    var url = OPINION_URL + "/getbyproduct"
    var data = {
        id: productId
    }
    
    jQuery.ajaxSetup({async:false});
    $.get(url, data,  function(data) {
        console.log(data);
        $.each(data, function (key, opinion) {
            showReview(opinion);
        });
    })
    .fail(function() {
        displayMessage("Ocorreu um erro :(");
    });
    jQuery.ajaxSetup({async:true});
}

function showReview(opinion) {
    var html = '<div class="opinion">';

    html += '<h5>' + opinion.title + '</h5>';
    html += '<p>' + opinion.text + '</p><br>';
    html += '<p>Nota: ' + opinion.score + '</p>';
    
    html += '</div>';

    $('#reviews').append(html);

}

function sendReview(productId) {
    var title = $('#new-review-title').val();
    var score = $('#new-review-score').val();
    var opinion = $('#new-review-opinion').val();

    var url = OPINION_URL + "/insert"
    var data = {
        productid: productId,
        clientid: getUserId(),
        title: title,
        text: opinion,
        score: score
    }
    
    jQuery.ajaxSetup({async:false});
    $.get(url, data,  function(data) {
        console.log(data);
        location.reload();
    })
    .fail(function() {
        displayMessage("Ocorreu um erro ao enviar sua avaliação");
    });
    jQuery.ajaxSetup({async:true});
}

function showReviewForm (productId) {
    var url = CART_URL + '/alreadyBought';
    var userId = getUserId();

    if (!userId)
        return;

    var data = {
        productId: productId,
        clientId: userId
    }

    jQuery.ajaxSetup({async:false});
    $.get(url, data,  function(data) {
        if (data == true) {
            $('#reviews').append(CREATE_REVIEW);
        }
    })
    .fail(function() {
        displayMessage("Ocorreu um erro ao enviar sua avaliação");
    });
    jQuery.ajaxSetup({async:true});
}

$(document).ready(function () {
    var productId = findGetParameter("id");
    showProduct(productId);
    getReviews(productId);

    showReviewForm(productId);

    $('#addToCart').click(function () {
        addItem(productId);
    })

    $('#view-history').click(function () {
        window.location = '/app/admin/product-purchases.html?id=' + productId;
    })

    $('#send-review').click(function () {
        sendReview(productId);
    })

    $("#quantity").bind('keyup mouseup', function () {
        var quantity = parseInt(this.value);
        updateQuantity(productId, quantity);
    });
});