function dateIsEqual(date, day, month, year) {
    return (date.getDate() == day && (date.getMonth() + 1) == month && date.getFullYear() == year)
}

function showPurchase(purchase, date) {
    var element = '<tr><td>' + purchase.id + '</td>';
    element += '<td>' + date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear() + '</td>';
    element += '<td>' + convertStatus(purchase.status) + '</td>';
    element += '<td>' + convertDeliveryType(purchase.deliveryType) + '</td>';
    element += '<td>R$' + purchase.totalPrice.toFixed(2) + '</td>';
    element += '<td>' + purchase.client.name + '</td>';
    element += '<td><button type="button" class="btn col-md-auto"><a class="buttom-link" href="/app/purchase.html?pId=' + purchase.id + '&cId=' + purchase.client.id + '">Ver Compra</a></button></td>';
    $('#purchases').append(element);
}

function showPurchases(day, month, year, haveToFilter) {
    var url = CART_URL + "/getall";
    var date = null;

    $.get(url,  function(data) {
        $.each(data, function (key, val) {
            date = new Date(val.date);

            if (haveToFilter) {
                if (dateIsEqual(date, day, month, year))
                    showPurchase(val, date);
            } else {
                showPurchase(val, date);
            }
            console.log(val);
        });
    })
    .fail(function() {
        displayMessage("Nenhuma compra encontrada");
    });
}

$(document).ready(function () {
    var day = parseInt(findGetParameter('day'));
    var month = parseInt(findGetParameter('month'));
    var year = parseInt(findGetParameter('year'));

    var haveToFilter = (!isNaN(day));

    if (haveToFilter) {
        $('#title').append(' de ' + day + '/' + month + '/' + year)
    }
    
    showPurchases(day, month, year, haveToFilter);
});