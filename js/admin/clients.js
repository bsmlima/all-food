function showClient(client) {
    var clientHtml = '<tr style="cursor:pointer;" onclick="document.location = ' + "'client-purchases.html?id=" + client.id + "'" + ';"><td>' + client.id + '</td>';
    clientHtml += '<td>' + client.name + '</td>';
    clientHtml += '<td>' + client.email + '</td>';
    clientHtml += '<td>' + client.cpf + '</td></tr>';
    $('#clients').append(clientHtml);
}

function showClients(text) {
    var url = CLIENT_URL + "/search"
    var data = {
        text: text
    }
    
    $.get(url, data,  function(data) {
        $('#clients').html("");
        $.each(data, function (key, client) {
            showClient(client);
        });
    })
    .fail(function() {
        displayMessage("Ocorreu um erro :(");
    });
}

function filter() {
    $('#clients').html("");
}

$(document).ready(function () {
    var text = "";

    showClients();

    $('#client-search').keyup(function() {
        text = $('#client-search').val();
        showClients(text);
    });
})