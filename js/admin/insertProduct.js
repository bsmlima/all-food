// Should have server hostname or ip
const HOST = "http://localhost:8080/product";

function insertRestrictions(product_id, restrictions_ids) {
    $.each(restrictions_ids, function (key, id) {
        var url = HOST + "/addRestriction";
        var data = {
            id: product_id,
            restrictionid: id
        }
        $.get(url, data,  function(data) {
            console.log("Restrição criada com sucesso");
        })
        .fail(function() {
            console.log("Erro ao adicionar as restrições");
        });
    });
}

function submitNewProduct(name, price, quantity, category_id, restrictions_ids) {
    var data = {
        name: name,
        price: price,
        quantityInStock: quantity,
        categoryId: category_id
    }
    $.ajax({
        url: HOST + "/insert",
        data: data,
        async: false,
        dataType: "json",
        crossDomain: true,
        success: function (data) {
            insertRestrictions(data.id, restrictions_ids);
            displayMessage("Produto criado com sucesso");
        },
        error: function(result) {
            displayMessage("Erro ao criar o produto");
        }
    });
};

function addCategories () {
    $.ajax({
        url: "http://localhost:8080/category/list",
        async: false,
        dataType: "json",
        crossDomain: true,
        success: function (data) {
            var option = '';
            $.each(data, function (key, val) {
                option = '<option value="' + val.id + '">' + val.name + '</option>';
                $('#category').append(option);
            });
        },
        error: function(result) {
            displayMessage("Data not found");
        }
    });
}

function addRestrictions () {
    $.ajax({
        url: "http://localhost:8080/restriction/list",
        async: false,
        dataType: "json",
        crossDomain: true,
        success: function (data) {
            var option = '';
            $.each(data, function (key, val) {
                option = '<option value="' + val.id + '">' + val.name + '</option>';
                $('#restriction').append(option);
            });
        },
        error: function(result) {
            displayMessage("Data not found");
        }
    });
}


// When HTML file is completely loaded
$(document).ready(function () {
    addCategories();
    addRestrictions();
    
    $("#submitProductButton").click(function () {
        event.preventDefault();
        var product_name = document.getElementById("inputName").value;
        var product_price = document.getElementById("inputPrice").value;
        var product_quantity = document.getElementById("inputQuantity").value;
        var product_category_id = $('#category').find(":selected").val();
        var product_restrictions_ids = $('#restriction').val();
        
        submitNewProduct(product_name, product_price, product_quantity, product_category_id, product_restrictions_ids);
    });
});