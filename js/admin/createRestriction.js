const URL = "http://localhost:8080/restriction/insert";

function createRestriction() {
    var name = $('#inputName').val();

    var data = {
        name: name
    }

    $.get(URL, data,  function () {
        displayMessage("Restrição criada com sucesso");
        $('#inputName').val('')
    })
    .fail(function () {
        displayMessage("Ocorreu um erro ao criar a restrição :(");
    });
}

$(document).ready( function() {
    $('#submitRestrictionButton').click(function () {
        event.preventDefault();
        createRestriction();
    })
});