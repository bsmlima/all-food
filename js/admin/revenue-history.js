// Should have server hostname or ip
const HOST = "http://localhost:8080/revenuehistory/getAll";

function showRevenue(revenue) {
    var day = revenue.date.substr(0,2);
    var month = revenue.date.substr(3,2);
    var year = revenue.date.substr(6,4);
    var date = "day=" + day + "&month=" + month + "&year=" + year; 
    var revenueHtml = '<tr style="cursor:pointer;" onclick="document.location = ' + "'/app/admin/purchases.html?" + date + "'" + ';">';
    revenueHtml += '<td>' + revenue.date + '</td>';
    revenueHtml += '<td>R$' + revenue.revenue.toFixed(2) + '</td></tr>';
    $('#revenue').append(revenueHtml);
}

function showRevenueHistory() {
    var url = HOST;
    
    $.get(url, function(data) {
        $.each(data, function (key, revenue) {
            showRevenue(revenue);
        });
    })
    .fail(function() {
        displayMessage("Ocorreu um erro :(");
    });
}

// When HTML file is completely loaded
$(document).ready(function () {
    showRevenueHistory();
});