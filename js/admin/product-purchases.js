function totalPrice(purchase) {
    var price = 0;
    var url = CART_URL + "/getitemsbypurchase";
    var data = {
        purchaseId: purchase.id
    }
    jQuery.ajaxSetup({async:false});
    $.get(url, data,  function(purchaseItems) {
        $.each(purchaseItems, function (key, val) {
            price += val.price * val.quantity;
        });
    })
    .fail(function() {
        displayMessage("Erro ao buscar compras");
    });
    jQuery.ajaxSetup({async:true});

    if (purchase.deliveryType == "AT_HOME")
        price += price * 0.10;
    return price;
}

function showPurchase(purchase) {
    console.log(purchase);
    
    var value = totalPrice(purchase).toFixed(2);
    var element = '<tr><td>' + purchase.id + '</td>';
    element += '<td>' + purchase.date.substr(0,10) + '</td>';
    element += '<td>' + convertStatus(purchase.status) + '</td>';
    element += '<td>' + convertDeliveryType(purchase.deliveryType) + '</td>';
    element += '<td>R$' + value + '</td>';
    element += '<td>' + purchase.client.name + '</td>';
    element += '<td><button type="button" class="btn col-md-auto"><a class="buttom-link" href="/app/purchase.html?pId=' + purchase.id + '&cId=' + purchase.client.id + '">Ver Compra</a></button></td>';
    $('#purchases').append(element);
}

function showPurchases() {
    var url = PRODUCT_URL + "/getHistory";
    var data = {
        productId: findGetParameter('id')
    }
    $.get(url, data,  function(data) {
        $.each(data, function (key, val) {
            showPurchase(val);
        });
    })
    .fail(function() {
        displayMessage("Erro ao buscar compras");
    });
}

function showProductName() {
    var url = PRODUCT_URL + '/getbyid';
    var data = {
        id: findGetParameter('id')
    }
    $.get(url, data,  function(product) {
        $('#title').text("Vendas de " + product.name);
    })
    .fail(function() {
        displayMessage("Ocorreu um erro ao buscar o produto");
    });
}

$(document).ready(function () {
    showPurchases();
    showProductName();
})