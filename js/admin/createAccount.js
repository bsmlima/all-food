const URL = "http://localhost:8080/admin/insert";

function createAdmin() {
    
    var name = $("#signUpNameAdm").val();
    var cpf = $("#signUpCpfAdm").val();
    var email = $("#signUpEmailAdm").val();
    var password = $("#signUpPasswordAdm").val();

    console.log(name);
    console.log(cpf);
    console.log(email);
    console.log(password);
    
    
    if (!name || !cpf || !email || !password) {
        displayMessage("Preencha todos os campos para continuar");
    } else {
        var data = {
            name: name,
            cpf: cpf,
            email: email,
            password: password
        }
        $.get(URL, data,  function(data) {
            displayMessage("Administrador cadastrado com sucesso");
        })
        .fail(function() {
            displayMessage("Ocorreu um erro :(");
        });
    }
}

$(document).ready(function () {
    
    $("#submitAccountButton").click(function () {
        event.preventDefault();
        createAdmin();
    });
});