const URL = "http://localhost:8080/category/insert";

function createCategory() {
    var name = $('#inputName').val();

    var data = {
        name: name
    }

    $.get(URL, data,  function () {
        displayMessage("Categoria criada com sucesso");
        $('#inputName').val('')
    })
    .fail(function () {
        displayMessage("Ocorreu um erro ao criar a categoria :(");
    });
}

$(document).ready( function() {
    $('#submitCategoryButton').click(function () {
        event.preventDefault();
        createCategory();
    })
});