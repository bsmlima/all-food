// Should have server hostname or ip
const HOST = "http://localhost:8080/purchaseitem";


function submitNewPurchaseItem(product_id, quantity, purchase_id) {
    $.ajax({
        url: HOST + "/insert?productId=" + product_id + "&quantity=" + quantity + "&purchaseId=" + purchase_id,
        async: false,
        dataType: "json",
        crossDomain: true,
        success: function (data) {
            displayMessage("Added");
        },
        error: function(result) {
            displayMessage("Couldn't submit new purchase item");
        }
    });
};

function addProductsOptions () {
    $.ajax({
        url: "http://localhost:8080/product/search",
        async: false,
        dataType: "json",
        crossDomain: true,
        success: function (data) {
            var option = '';
            $.each(data, function (key, val) {
                option = '<option value="' + val.product.id + '">' + val.product.name + '</option>';
                $('#products').append(option);
            });
        },
        error: function(result) {
            displayMessage("Data not found");
        }
    });
}

function addPurchasesOptions () {
    $.ajax({
        url: "http://localhost:8080/purchase/getByClientId?clientId=1",
        async: false,
        dataType: "json",
        crossDomain: true,
        success: function (data) {
            var option = '';
            $.each(data, function (key, val) {
                option = '<option value="' + val.id + '">Compra ' + val.id + '</option>';
                $('#purchases').append(option);
            });
        },
        error: function(result) {
            displayMessage("Data not found");
        }
    });
}

// When HTML file is completely loaded
$(document).ready(function () {
    addProductsOptions();
    addPurchasesOptions();
    $("#submitPurchaseItemButton").click(function () {
        event.preventDefault();
        var product_quantity = document.getElementById("inputQuantity").value;
        var product_id = $('#products').find(":selected").val();
        var purchase_id = $('#purchases').find(":selected").val();

        submitNewPurchaseItem(product_id, product_quantity, purchase_id);
    });
});