const LOGIN_URL = "http://localhost:8080/person/login";
const CART_URL = "http://localhost:8080/purchase";
const CART_ITEM_URL = "http://localhost:8080/purchaseitem";
const PRODUCT_URL = "http://localhost:8080/product";
const CLIENT_URL = "http://localhost:8080/client";
const OPINION_URL = "http://localhost:8080/opinion";
const SIDEBAR = `
<!-- Admin sidebar -->
<div class="sidenav">
    <div class="side-title">
        <h6 class="text-center">Ambiente Administrativo</h6>
    </div>
    <div class="side-links">
        <a href="/app/admin/insertProduct.html">Inserir produto</a>
        <a href="/app/admin/createCategory.html">Inserir categoria</a>
        <a href="/app/admin/createRestriction.html">Inserir alérgico</a>
        <div class="divisor"></div>
        <a href="/app/admin/revenue-history.html">Faturamento</a>
        <a href="/app/admin/purchases.html">Vendas</a>
        <a href="/app/admin/clients.html">Clientes</a>
        <a href="/app/admin/createAccount.html">Criar conta</a>
    </div>
</div>`;

var messagesCount = 0

function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
          tmp = item.split("=");
          if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}

function displayMessage(message) {
    var currentMessage = messagesCount;
    var divId = '#message-' + currentMessage;
    var messageDiv = '<div class="message" id="message-' + messagesCount++ + '">' + message + '</div>';
    $('.messages-box').append(messageDiv);
    $(divId).fadeIn("fast");
    setTimeout(function(){
        $(divId).fadeOut("slow");
      }, 5000)
}

function login(email, password) {
    var data = {
        email: email,
        password: password
    }
    $.get(LOGIN_URL, data,  function(login) {
        if (login.success) {
            setCookie("userId", login.message.id);
            setCookie("userEmail", login.message.email);
            setCookie("userName", login.message.name);
            setCookie("isAdmin", login.admin);
            if (!isAdmin()) {
                if (hasPendingCart()) {
                    getCart();
                } else {
                    searchCart();
                }
            }
            location.reload();
        } else {
            displayMessage(login.message);
        }
    })
    .fail(function() {
        displayMessage("Ocorreu um erro ao fazer login :(");
    });
}

function logout() {
    eraseCookie("userId");
    eraseCookie("userEmail");
    eraseCookie("userName");
    eraseCookie("isAdmin");
    eraseCookie('cartId');
    eraseCookie('pendingCart');
    window.location = "/app/home.html";
}

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}

function setCookie(name, value) {
    var cookie = name + "=" + value + "; path=/;";
    document.cookie = cookie
}

function eraseCookie(name) {   
    document.cookie = name+'=; Max-Age=-99999999; path=/;';  
}

function getUserId() { 
    return getCookie('userId');
}

function getUserEmail() { 
    return getCookie('userEmail');
}

function getUserName() { 
    return getCookie('userName');
}

function isAdmin() {
    return (getCookie('isAdmin') == 'true');
}

function isLoggedIn() {
    return getUserId() != undefined;
}

function hasPendingCart() {
    return (getCookie('pendingCart') == 'true');
}

function addProduct(product, totalSold, span) {
    var productHtml = '<div class="col-md-' + span + '"><div class="product"> ';
    productHtml += '<h6 class="product-title">' + product.name + '</h6>';
    productHtml += '<p>R$ ' + product.price + '</p>';
    productHtml += '<p>Estoque: ' + product.quantityInStock + ' unidades</p>';
    productHtml += '<p>Categoria: ' + product.category.name + '</p>';
    productHtml += '<p>Vendidos: ' + totalSold + '</p><br><br>';
    productHtml += `<div class="button-container"
                        <div class="row justify-content-center">
                            <a class="buttom-link" href="/app/product.html?id=` + product.id + `"><button type="button" class="btn col-md-8">Ver Produto</button></a>
                        </div>
                    </div>`;
    productHtml += '</div></div>';
    $('#products').append(productHtml);
}

function getCart() {
    var url = CART_URL + "/setClient";
    var data = {
        clientId: getUserId(),
        purchaseId: getCartId()
    }

    jQuery.ajaxSetup({async:false});
    $.get(url, data,  function(data) {
        console.log(data);
        setCookie('pendingCart', 'false');
    })
    .fail(function() {
        log("Ocorreu um erro ao criar o carrinho");
    });
    jQuery.ajaxSetup({async:true});
}

function searchCart() {
    var url = CART_URL + "/getByClientId";
    var data = {
        clientId: getUserId()
    }

    jQuery.ajaxSetup({async:false});
    $.get(url, data,  function(data) {
        console.log(data);
        $.each(data, function (key, purchase) {
            if (purchase.status == "AT_CART") {
                setCookie('cartId', purchase.id);
            }
        });
    })
    .fail(function() {
        log("Ocorreu um erro ao criar o carrinho");
    });
    jQuery.ajaxSetup({async:true});
}

function createCart() {
    var url = CART_URL + "/insert";
    var data = {
        clientId: getUserId(),
        deliveryType: 1
    }
    jQuery.ajaxSetup({async:false});
    $.get(url, data,  function(data) {
        console.log(data);
        setCookie('cartId', data.message.id);
        if (!isLoggedIn())
            setCookie('pendingCart', 'true');
    })
    .fail(function() {
        displayMessage("Ocorreu um erro ao criar o carrinho");
    });
    jQuery.ajaxSetup({async:true});
}

function getCartId() {
    return getCookie('cartId');
}

function addItem(productId) {

    if (getCartId() == undefined) {
        createCart();
    }
    
    var url = CART_ITEM_URL + "/insert";
    var cartId = getCartId();
    
    var data = {
        productId: productId,
        purchaseId: cartId,
        quantity: 1
    }
    $.get(url, data,  function(data) {
        displayMessage("Produto adicionado ao carrinho");
    })
    .fail(function() {
        displayMessage("Erro ao adicionar ao carrinho");
    });
}

function convertDeliveryType(deliveryType) {
    if (deliveryType == 'AT_HOME') {
        return 'Em casa';
    } else {
        return 'Retirar na loja'
    }
}

function convertStatus(status) {
    if (status == 'AT_CART') return 'Carrinho'
    else if (status == 'FINISHED') return 'Finalizada'
    else if (status == 'WAITING_FOR_PAYMENT') return 'Aguardando pagamento'
    else if (status == 'CANCELED') return 'Cancelada'
}
$(document).ready(function () {
    if (isAdmin()) {
        $('body').append(SIDEBAR);
        $('#main').addClass('main');
        $('.messages-box').addClass('main');
    }
});