function totalPrice (purchase, purchaseItems) {
    var price = 0;
    $.each(purchaseItems, function (key, val) {
        price += val.price * val.quantity;
    });

    if (purchase.deliveryType == "AT_HOME") {
        price += price * 0.10
    }
    return price;
}

function showPurchaseItem(purchaseItem) {
    var product = '<tr><td>' + purchaseItem.product.name + '</td>';
    product += '<td>R$' + purchaseItem.price + '</td>';
    product += '<td>' + purchaseItem.quantity + '</td></tr>';
    $('#products').append(product);
}

function showPurchase(purchase, purchaseItems) {
    var price = totalPrice(purchase, purchaseItems).toFixed(2);
    
    $.each(purchaseItems, function (key, val) {
        showPurchaseItem(val);
    });
    $('.page-title').text('Compra ' + purchase.id);
    $('#purchase').prepend('<h6>Status: ' + convertStatus(purchase.status) + '</h6><br>');
    $('#purchase').prepend('<h6>Entrega: ' + convertDeliveryType(purchase.deliveryType) + '</h6>');
    $('#purchase').prepend('<h6>Total: R$' + price + '</h6>');
}

function getPurchaseItems(purchase) {
    var url = CART_URL + "/getitemsbypurchase";
    var data = {
        purchaseId: purchase.id
    }
    $.get(url, data,  function(purchaseItems) {
        showPurchase(purchase, purchaseItems)
    })
    .fail(function() {
        displayMessage("Erro ao buscar produtos");
    });
}

function getPurchase(purchaseId, clientId) {
    var url = CART_URL + "/getByClientId";
    var data = {
        clientId: clientId
    }
    $.get(url, data,  function(data) {
        $.each(data, function (key, val) {
            if (val.id == purchaseId) {
                getPurchaseItems(val);
            }            
        });
    })
    .fail(function() {
        displayMessage("Erro ao buscar compra");
    });
}

function confirmPayment(purchaseId) {
    var url = CART_URL + "/finishpurchase";
    var data = {
        purchaseId: purchaseId
    }
    $.get(url, data,  function(data) {
        if (data.success) {
            displayMessage("Pagamento confirmado com sucesso");
        } else {
            displayMessage(data.message);
        }
        
    })
    .fail(function() {
        displayMessage("Erro ao buscar produtos");
    });
}

function cancelPurchase(purchaseId) {
    var url = CART_URL + "/cancelpurchase";
    var data = {
        purchaseId: purchaseId
    }
    $.get(url, data,  function(data) {
        if (data.success) {
            displayMessage("Compra cancelada com sucesso");
        } else {
            displayMessage(data.message);
        }
    })
    .fail(function() {
        displayMessage("Erro ao buscar produtos");
    });
}

$(document).ready(function () {
    var purchaseId = findGetParameter("pId");
    var clientId = findGetParameter("cId");
    getPurchase(purchaseId, clientId);

    $('#confirmPayment').click(function () {
        confirmPayment(purchaseId);
    })

    $('#cancelPurchase').click(function () {
        cancelPurchase(purchaseId);
    })
});