function processPurchase(deliveryType) {
    var url = CART_URL + "/processpurchase"
    var cartId = getCartId();

    if (!cartId) {
        displayMessage("Adicione produtos ao carrinho antes de confirmar a compra")
        return;
    }
    
    if (!isLoggedIn()) {
        displayMessage("Faça login antes de confirmar a compra");
        return;
    }

    var data = {
        purchaseId: cartId,
        deliveryType: deliveryType
    }
    
    $.get(url, data,  function(data) {
        if (data.success) {
            displayMessage("Compra confirmada com sucesso, aguardando pagamento.");
            displayMessage("Acesse sua lista de compras para acompanhar o pedido");
            eraseCookie('cartId');
            console.log(data);
        } else {
            displayMessage("Erro ao confirmar a compra");
        }
    })
    .fail(function() {
        displayMessage("Ocorreu um erro :(");
    });
}

function showCartItem(purchaseItem) {
    var product = '<tr><td>' + purchaseItem.product.name + '</td>';
    product += '<td>R$' + purchaseItem.price + '</td>';
    product += '<td><input id="' + purchaseItem.id + '" class="form-control form-control-sm item-quantity" type="number" step="1" min="1" value="' + purchaseItem.quantity + '">';
    product += '<button type="button" class="delete-btn" id="delete-' + purchaseItem.id + '"><span class="fa fa-times" id="search-icon"></span></button></td></tr>';
    $('#products').append(product);
}

function showCartItems() {
    var url = CART_URL + "/getitemsbypurchase"
    var cartId = getCartId();

    if (!cartId) {
        displayMessage("Seu carrinho não possui nenhum produto")
        return;
    }

    var data = {
        purchaseId: cartId
    }
    jQuery.ajaxSetup({async:false});
    $.get(url, data,  function(data) {
        $.each(data, function (key, val) {
            showCartItem(val);
        });
    })
    .fail(function() {
        displayMessage("Ocorreu um erro :(");
    });
    jQuery.ajaxSetup({async:true});
}

function updateQuantity(id, quantity) {
    var url = CART_ITEM_URL + "/updatequantity"
    var data = {
        purchaseitemid: id,
        quantity: quantity
    }
    
    $.get(url, data,  function(data) {
        console.log(data);
    })
    .fail(function() {
        displayMessage("Ocorreu um erro :(");
    });
}

function deleteItem(id) {
    var url = CART_ITEM_URL + "/delete"
    var data = {
        purchaseitemid: id
    }
    
    $.get(url, data,  function(data) {
        console.log(data);
    })
    .fail(function() {
        displayMessage("Ocorreu um erro :(");
    });
}

$(document).ready(function () {
    showCartItems();
    var deliveryType = "";

    $('#processAccount').click(function () {
        deliveryType = $("#delivery").val();
        if (deliveryType == "") {
            displayMessage("Selecione um tipo de entrega");
        } else {
            processPurchase(deliveryType);
        }
    })

    $(".item-quantity").bind('keyup mouseup', function () {
        var id = parseInt(this.id);
        var quantity = parseInt(this.value);
        updateQuantity(id, quantity);
    });

    $('.delete-btn').click(function () {
        var id = parseInt(this.id.slice(7));
        deleteItem(id);
        location.reload();
    })
    
    
})