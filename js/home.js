// Should have server hostname or ip
const HOST = "http://localhost:8080/product";

function clearProducts() {
    $('#products').html("");
}

function filter() {
    var url = HOST + '/search';
    var isDesc = $('input[name=order]:checked').val();
    var restrictionsIds = $('#restrictions').val();
    var categoryId = $('#categories').find(":selected").val();
    var orderField = $('#orderField').find(":selected").val();
    var text = $('#search').val();

    var data = {
        orderField: orderField,
        isDesc: isDesc,
        text: text,
        categoryId: categoryId,
        restrictions: restrictionsIds
    }

    $.ajax({
        url: url,
        data: data,
        async: false,
        dataType: "json",
        crossDomain: true,
        beforeSend: function(jqXHR, settings) {
            console.log(settings.url);
        },
        success: function (data) {
            
            clearProducts();
            $.each(data, function (key, val) {
                addProduct(val.product, val.productHistory.totalSold, 4);
            });
        },
        error: function(result) {
            displayMessage("Data not found");
        }
    });
    window.history.pushState({}, null, 'home.html?search=' + text); // URL live update
}

function addCategoriesOptions () {
    $.ajax({
        url: "http://localhost:8080/category/list",
        async: false,
        dataType: "json",
        crossDomain: true,
        success: function (data) {
            var option = '';
            $.each(data, function (key, val) {
                option = '<option value="' + val.id + '">' + val.name + '</option>';
                $('#categories').append(option);
            });
        },
        error: function(result) {
            displayMessage("Data not found");
        }
    });
}

function addRestrictionsOptions () {
    $.ajax({
        url: "http://localhost:8080/restriction/list",
        async: false,
        dataType: "json",
        crossDomain: true,
        success: function (data) {
            var option = '';
            $.each(data, function (key, val) {
                option = '<option value="' + val.id + '">' + val.name + '</option>';
                $('#restrictions').append(option);
            });
        },
        error: function(result) {
            displayMessage("Data not found");
        }
    });
}

// When HTML file is completely loaded
$(document).ready(function () {
    var text = findGetParameter('search');

    if (!text) {
        text = '';
    }

    $.ajax({
        url: HOST + '/search?text=' + text,
        async: false,
        dataType: "json",
        crossDomain: true,
        success: function (data) {
            $.each(data, function (key, val) {
                addProduct(val.product, val.productHistory.totalSold, 4);
            });
        },
        error: function(result) {
            displayMessage("Data not found");
        }
    });

    addCategoriesOptions();
    addRestrictionsOptions();

    $('input[name=order]').change(function () {
        filter();
    });
    $('#categories').change(function () {
        filter();
    });
    $('#restrictions').change(function () {
        filter();
    });
    $('#orderField').change(function () {
        filter();
    });
    $(document).keyup(function(e){
        filter();
    });
});