function totalPrice(purchase) {
    var price = 0;
    var url = CART_URL + "/getitemsbypurchase";
    var data = {
        purchaseId: purchase.id
    }
    jQuery.ajaxSetup({async:false});
    $.get(url, data,  function(purchaseItems) {
        $.each(purchaseItems, function (key, val) {
            price += val.price * val.quantity;
        });
    })
    .fail(function() {
        displayMessage("Erro ao buscar compras");
    });
    jQuery.ajaxSetup({async:true});

    if (purchase.deliveryType == "AT_HOME") {
        price += price * 0.10;
    }
    return price;
}

function showPurchase(purchase) {
    var value = totalPrice(purchase).toFixed(2);
    var element = '<tr><td>' + purchase.id + '</td>';
    element += '<td>' + convertStatus(purchase.status) + '</td>';
    element += '<td>' + convertDeliveryType(purchase.deliveryType) + '</td>';
    element += '<td>R$' + value + '</td>';
    element += '<td><button type="button" class="btn col-md-auto"><a class="buttom-link" href="/app/purchase.html?pId=' + purchase.id + '&cId=' + getUserId() + '">Ver Compra</a></button></td>';
    $('#purchases').append(element);
}

function showPurchases() {
    var url = CART_URL + "/getByClientId";
    var data = {
        clientId: getUserId()
    }
    $.get(url, data,  function(data) {
        $.each(data, function (key, val) {
            showPurchase(val);
            console.log(val);
            
        });
    })
    .fail(function() {
        displayMessage("Você não possui compras");
    });
}

$(document).ready(function () {
    showPurchases();
})